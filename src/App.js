import React from "react";
import './App.css';
import MaterialUIDrawer from './drawer';
import {
    BrowserRouter as Router, Switch, Route
} from "react-router-dom";
import SubscriptionTab from './components/SubscriptionTab';
import DepartmentTab from './components/DepartmentTab';
import BikeTab from "./components/BikeTab";
import MemberTab from "./components/MemberTab";
import EmployeeTab from "./components/EmployeeTab";
import ScheduleTab from "./components/ScheduleTab";
import CompetitionResultTab from "./components/CompetitionResultTab";
import GroupTab from "./components/GroupTab";
import SubscriptionHistoryTab from "./components/SubscriptionHistoryTab";
import RentalTab from "./components/RentalTab";
import HomeTab from "./components/HomeTab";

function App() {
    return (
        <div className="App">
            <Router>
                <MaterialUIDrawer/>
                <Switch>
                    <Route exact path='/' render=
                        {props => <HomeTab {...props} />}/>
                    <Route exact path='/departments' render=
                        {props => <DepartmentTab {...props} /> }/>
                    <Route exact path='/subscriptions' render=
                        {props => <SubscriptionTab {...props} /> }/>
                    <Route exact path='/bikes' render=
                        {props => <BikeTab {...props} /> }/>
                    <Route exact path='/members' render=
                        {props => <MemberTab {...props} /> }/>
                    <Route exact path='/employees' render=
                        {props => <EmployeeTab {...props} /> }/>
                    <Route exact path='/groups' render=
                        {props => <GroupTab {...props} /> }/>
                    <Route exact path='/schedule' render=
                        {props => <ScheduleTab {...props} /> }/>
                    <Route exact path='/results' render=
                        {props => <CompetitionResultTab {...props} /> }/>
                    <Route exact path='/history' render=
                        {props => <SubscriptionHistoryTab {...props} /> }/>
                    <Route exact path='/rentals' render=
                        {props => <RentalTab {...props} /> }/>
                </Switch>
            </Router>
        </div>
    );
}

export default App;