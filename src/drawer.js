import React from 'react';
import { Drawer, Divider, IconButton }
    from '@material-ui/core';
import { List, ListItem, ListItemIcon, ListItemText }
    from '@material-ui/core';
import PlaceIcon from '@material-ui/icons/Place';
import ReorderIcon from '@material-ui/icons/Reorder';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import PersonIcon from '@material-ui/icons/Person';
import GroupIcon from '@material-ui/icons/Group';
import EventIcon from '@material-ui/icons/Event';
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import HistoryIcon from '@material-ui/icons/History';
import { Link } from 'react-router-dom';

const styles = {
    sideNav: {
        zIndex: 3,
        marginTop: '20px',
        marginLeft: '15px',
        position: 'fixed',
    },
    link: {
        color: 'black',
        textDecoration: 'none',
    }
};

export default class MaterialUIDrawer
    extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDrawerOpened: false,
        };
    }
    toggleDrawerStatus = () => {
        this.setState({
            isDrawerOpened: true,
        })
    }
    closeDrawer = () => {
        this.setState({
            isDrawerOpened: false,
        })
    }
    render() {
        const { isDrawerOpened } = this.state;
        return (
            <div>
                <div style={styles.sideNav}>
                    <IconButton onClick={this.toggleDrawerStatus}>
                        {!isDrawerOpened ? <ReorderIcon /> : null }
                    </IconButton>
                </div>
                <Divider/>
                <Drawer
                    variant="temporary"
                    open={isDrawerOpened}
                    onClose={this.closeDrawer}
                >
                    <Link to='/departments' style={styles.link}>
                        <List>
                            <ListItem button key='Departments'>
                                <ListItemIcon><PlaceIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Departments' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/subscriptions' style={styles.link}>
                        <List>
                            <ListItem button key='Subscriptions'>
                                <ListItemIcon><CardMembershipIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Subscriptions' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/bikes' style={styles.link}>
                        <List>
                            <ListItem button key='Bikes'>
                                <ListItemIcon><DirectionsBikeIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Bikes' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/members' style={styles.link}>
                        <List>
                            <ListItem button key='Members'>
                                <ListItemIcon><PersonOutlineIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Members' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/employees' style={styles.link}>
                        <List>
                            <ListItem button key='Employee'>
                                <ListItemIcon><PersonIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Employee' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/groups' style={styles.link}>
                        <List>
                            <ListItem button key='Groups'>
                                <ListItemIcon><GroupIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Groups' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/schedule' style={styles.link}>
                        <List>
                            <ListItem button key='Schedule'>
                                <ListItemIcon><EventIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Schedule' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/results' style={styles.link}>
                        <List>
                            <ListItem button key='Competition results'>
                                <ListItemIcon><EmojiEventsIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Competition results' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/history' style={styles.link}>
                        <List>
                            <ListItem button key='Subscription history'>
                                <ListItemIcon><HistoryIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Subscription history' />
                            </ListItem>
                        </List>
                    </Link>
                    <Link to='/rentals' style={styles.link}>
                        <List>
                            <ListItem button key='Rentals'>
                                <ListItemIcon><HistoryIcon/>
                                </ListItemIcon>
                                <ListItemText primary='Rentals' />
                            </ListItem>
                        </List>
                    </Link>
                </Drawer>
            </div>
        );
    }
}
