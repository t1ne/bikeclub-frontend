import React, { useState, useEffect } from 'react';
import { forwardRef } from 'react';
import Grid from '@material-ui/core/Grid'

import MaterialTable from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import axios from 'axios'
import Alert from '@material-ui/lab/Alert';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const api = axios.create({
    baseURL: `http://localhost:8080/api`
})

function ScheduleTab() {

    const [data, setData] = useState([]); //table data
    const [employees, setEmployees] = useState({});
    const [employeeLinks, setEmployeeLinks] = useState({});

    const columns = [
        {title: 'Name', field: 'event.name'},
        {title: 'Type', field: 'event.type.name'},
        {title: 'Price', field: 'event.price', type: 'numeric'},
        {title: 'Organizer', field: 'organizer.id', lookup: employees},
        {title: 'Date', field: 'date', type: 'date'},
        {title: 'Created', field: 'createdAt', editable: 'never', type: 'datetime'},
        {title: 'Updated', field: 'updatedAt', editable: 'never', type: 'datetime'}
    ]

    //for error handling
    const [iserror, setIserror] = useState(false)
    const [errorMessages, setErrorMessages] = useState([])

    useEffect(() => {
        api.get("/schedules")
            .then(async res => {
                let schedules = res.data._embedded.schedules
                for (const x of schedules) {
                    x.event = (await axios.get(x._links.event.href)).data
                    x.organizer = (await axios.get(x._links.organizer.href)).data
                    x.attendees = (await axios.get(x._links.attendees.href)).data._embedded.members
                    for (const attendee of x.attendees) {
                        attendee.skillLevel = (await axios.get(attendee._links.skillLevel.href)).data
                    }
                }
                setData(schedules);

                let allEmployees = (await api.get("/employees")).data._embedded.employees
                let employeeObj = {}
                let employeeLinks = {}
                allEmployees.forEach(x => {
                    employeeObj[x.id] = x.firstName + ' ' + x.lastName;
                    employeeLinks[x.id] = x._links.self.href
                });
                setEmployees(employeeObj)
                setEmployeeLinks(employeeLinks)
            })
            .catch(error=>{
                console.log("Error")
            })
    }, [])

    const handleRowUpdate = (newData, oldData, resolve) => {
        let organizerId = newData.organizer.id
        delete newData.organizer
        axios.put(oldData._links.self.href, newData)
            .then(async res => {
                newData = res.data
                await axios.put(oldData._links.organizer.href, employeeLinks[organizerId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.organizer = (await axios.get(employeeLinks[organizerId])).data
                const dataUpdate = [...data];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Update failed! Server error"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowAdd = (newData, resolve) => {
        let organizerId = newData.organizer.id
        delete newData.organizer
        api.post("/schedules", newData)
            .then(async res => {
                newData = res.data
                await axios.put(res.data._links.organizer.href, employeeLinks[organizerId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.organizer = (await axios.get(employeeLinks[organizerId])).data
                let dataToAdd = [...data];
                dataToAdd.push(newData);
                setData(dataToAdd);
                resolve()
                setErrorMessages([])
                setIserror(false)
            })
            .catch(error => {
                setErrorMessages(["Cannot add data. Server error!"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowDelete = (oldData, resolve) => {
        axios.delete(oldData._links.self.href)
            .then(res => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1)
                setData([...dataDelete]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Delete failed! Server error"])
                setIserror(true)
                resolve()
            })
    }

    const eventAttendees = (rowData) => {
        const columns = [
            {title: 'First name', field: 'firstName'},
            {title: 'Last name', field: 'lastName'},
            {title: 'Birth date', field: 'birthDate', type: 'date'},
            {title: 'Phone number', field: 'phoneNumber'},
            {title: 'Email', field: 'email'},
            {title: 'Skill level', field: 'skillLevel.name'},
            {title: 'Created', field: 'createdAt', editable: 'never', type: 'datetime'},
            {title: 'Updated', field: 'updatedAt', editable: 'never', type: 'datetime'}
        ]
        return <MaterialTable
            title={'Event attendees'}
            columns={columns}
            data={rowData.attendees}
            icons={tableIcons}/>
    }

    return (
        <Grid container spacing={1}>
            <Grid item xs={1}/>
            <Grid item xs={10}>
                <div>
                    {iserror &&
                    <Alert severity="error">
                        {errorMessages.map((msg, i) => {
                            return <div key={i}>{msg}</div>
                        })}
                    </Alert>
                    }
                </div>
                <MaterialTable
                    title="BikeClub Schedule"
                    data={data}
                    columns={columns}
                    icons={tableIcons}
                    detailPanel={eventAttendees}
                    editable={{
                        onRowUpdate: (newData, oldData) =>
                            new Promise((resolve) => {
                                handleRowUpdate(newData, oldData, resolve);
                            }),
                        onRowAdd: (newData) =>
                            new Promise((resolve) => {
                                handleRowAdd(newData, resolve)
                            }),
                        onRowDelete: (oldData) =>
                            new Promise((resolve) => {
                                handleRowDelete(oldData, resolve)
                            }),
                    }}
                />
            </Grid>
            <Grid item xs={1}/>
        </Grid>
    );
}

export default ScheduleTab;