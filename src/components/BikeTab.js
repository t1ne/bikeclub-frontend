import React, { useState, useEffect } from 'react';
import { forwardRef } from 'react';
import Grid from '@material-ui/core/Grid'

import MaterialTable from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import axios from 'axios'
import Alert from '@material-ui/lab/Alert';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const api = axios.create({
    baseURL: `http://localhost:8080/api`
})

function BikeTab() {

    const [data, setData] = useState([]); //table dat
    const [brands, setBrands] = useState({})
    const [brandLinks, setBrandLinks] = useState({})
    const [bikeTypes, setBikeTypes] = useState({})
    const [bikeTypeLinks, setBikeTypeLinks] = useState({})
    const [brakeTypes, setBrakeTypes] = useState({})
    const [brakeTypeLinks, setBrakeTypeLinks] = useState({})
    const [groupsets, setGropusets] = useState({})
    const [groupsetLinks, setGropusetLinks] = useState({})
    const [wheelsets, setWheelsets] = useState({})
    const [wheelsetLinks, setWheelsetLinks] = useState({})

    const columns = [
        {title: 'Brand', field: 'brand.id', lookup: brands},
        {title: 'Model', field: 'model'},
        {title: 'Type', field: 'type.id', lookup: bikeTypes},
        {title: 'Frame size', field: 'frameSize', type: 'numeric'},
        {title: 'Brake type', field: 'brakeType.id', lookup: brakeTypes},
        {title: 'Groupset', field: 'groupset.id', lookup: groupsets},
        {title: 'Wheelset', field: 'wheelset.id', lookup: wheelsets},
        {title: 'Weight', field: 'weight', type: 'numeric'},
        {title: 'Release year', field: 'releaseYear', type: 'numeric'},
        {title: 'Price per hour', field: 'pricePerHour', type: 'currency'},
        {title: 'Created', field: 'createdAt', editable: 'never', type: 'datetime'},
        {title: 'Updated', field: 'updatedAt', editable: 'never', type: 'datetime'}
    ]

    //for error handling
    const [iserror, setIserror] = useState(false)
    const [errorMessages, setErrorMessages] = useState([])

    useEffect(() => {
        api.get("/bikes")
            .then(async res => {
                let bikes = res.data._embedded.bikes
                for (const x of bikes) {
                    x.brand = (await axios.get(x._links.brand.href)).data
                    x.type = (await axios.get(x._links.type.href)).data
                    x.brakeType = (await axios.get(x._links.brakeType.href)).data
                    x.groupset = (await axios.get(x._links.groupset.href)).data
                    x.wheelset = (await axios.get(x._links.wheelset.href)).data
                }
                setData(bikes)

                let allBrands = (await api.get("/bikeBrands")).data._embedded.bikeBrands
                let brandObj = {}
                let brandLinks = {}
                allBrands.forEach(x => {
                    brandObj[x.id] = x.name;
                    brandLinks[x.id] = x._links.self.href
                });
                setBrands(brandObj)
                setBrandLinks(brandLinks)

                let allBikeTypes = (await api.get("/bikeTypes")).data._embedded.bikeTypes
                let typeObj = {}
                let typeLinks = {}
                allBikeTypes.forEach(x => {
                    typeObj[x.id] = x.name;
                    typeLinks[x.id] = x._links.self.href
                });
                setBikeTypes(typeObj)
                setBikeTypeLinks(typeLinks)

                let allBrakeTypes = (await api.get("/brakeTypes")).data._embedded.brakeTypes
                let brakeTypeObj = {}
                let brakeTypeLinks = {}
                allBrakeTypes.forEach(x => {
                    brakeTypeObj[x.id] = x.name;
                    brakeTypeLinks[x.id] = x._links.self.href
                });
                setBrakeTypes(brakeTypeObj)
                setBrakeTypeLinks(brakeTypeLinks)

                let allGroupsets = (await api.get("/groupsets")).data._embedded.groupsets
                let groupsetObj = {}
                let groupsetLinks = {}
                allGroupsets.forEach(x => {
                    groupsetObj[x.id] = x.name + ' ' + x.frontGearsCount + 'x' + x.rearGearsCount;
                    groupsetLinks[x.id] = x._links.self.href
                });
                setGropusets(groupsetObj)
                setGropusetLinks(groupsetLinks)

                let allWheelsets = (await api.get("/wheelsets")).data._embedded.wheelsets
                let wheelsetObj = {}
                let wheelsetLinks = {}
                allWheelsets.forEach(x => {
                    wheelsetObj[x.id] = x.name + ' ' + x.size + 'x' + x.width;
                    wheelsetLinks[x.id] = x._links.self.href
                });
                setWheelsets(wheelsetObj)
                setWheelsetLinks(wheelsetLinks)
            })
            .catch(error=>{
                console.log("Error")
            })
    }, [])

    const handleRowUpdate = (newData, oldData, resolve) => {
        let brandId = newData.brand.id
        delete newData.brand
        let typeId = newData.type.id
        delete newData.type
        let brakeTypeId = newData.brakeType.id
        delete newData.brakeType
        let groupsetId = newData.groupset.id
        delete newData.groupset
        let wheelsetId = newData.wheelset.id
        delete newData.wheelset
        axios.put(oldData._links.self.href, newData)
            .then(async res => {
                newData = res.data
                await axios.put(oldData._links.brand.href, brandLinks[brandId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.brand = (await axios.get(brandLinks[brandId])).data
                await axios.put(oldData._links.type.href, bikeTypeLinks[typeId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.type = (await axios.get(bikeTypeLinks[typeId])).data
                await axios.put(oldData._links.brakeType.href, brakeTypeLinks[brakeTypeId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.brakeType = (await axios.get(brakeTypeLinks[brakeTypeId])).data
                await axios.put(oldData._links.groupset.href, groupsetLinks[groupsetId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.groupset = (await axios.get(groupsetLinks[groupsetId])).data
                await axios.put(oldData._links.wheelset.href, wheelsetLinks[wheelsetId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.wheelset = (await axios.get(wheelsetLinks[wheelsetId])).data
                const dataUpdate = [...data];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Update failed! Server error"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowAdd = (newData, resolve) => {
        let brandId = newData.brand.id
        delete newData.brand
        let typeId = newData.type.id
        delete newData.type
        let brakeTypeId = newData.brakeType.id
        delete newData.brakeType
        let groupsetId = newData.groupset.id
        delete newData.groupset
        let wheelsetId = newData.wheelset.id
        delete newData.wheelset
        api.post("/bikes", newData)
            .then(async res => {
                newData = res.data
                await axios.put(res.data._links.brand.href, brandLinks[brandId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.brand = (await axios.get(brandLinks[brandId])).data;
                await axios.put(res.data._links.type.href, bikeTypeLinks[typeId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.type = (await axios.get(bikeTypeLinks[typeId])).data
                await axios.put(res.data._links.brakeType.href, brakeTypeLinks[brakeTypeId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.brakeType = (await axios.get(brakeTypeLinks[brakeTypeId])).data
                await axios.put(res.data._links.groupset.href, groupsetLinks[groupsetId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.groupset = (await axios.get(groupsetLinks[groupsetId])).data
                await axios.put(res.data._links.wheelset.href, wheelsetLinks[wheelsetId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.wheelset = (await axios.get(wheelsetLinks[wheelsetId])).data
                let dataToAdd = [...data];
                dataToAdd.push(newData);
                setData(dataToAdd);
                resolve()
                setErrorMessages([])
                setIserror(false)
            })
            .catch(error => {
                setErrorMessages(["Cannot add data. Server error!"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowDelete = (oldData, resolve) => {
        axios.delete(oldData._links.self.href)
            .then(res => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1)
                setData([...dataDelete]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Delete failed! Server error"])
                setIserror(true)
                resolve()
            })
    }


    return (
        <Grid container spacing={1}>
            <Grid item xs={1}/>
            <Grid item xs={10}>
                <div>
                    {iserror &&
                    <Alert severity="error">
                        {errorMessages.map((msg, i) => {
                            return <div key={i}>{msg}</div>
                        })}
                    </Alert>
                    }
                </div>
                <MaterialTable
                    title="BikeClub Bikes"
                    data={data}
                    columns={columns}
                    icons={tableIcons}
                    editable={{
                        onRowUpdate: (newData, oldData) =>
                            new Promise((resolve) => {
                                handleRowUpdate(newData, oldData, resolve);
                            }),
                        onRowAdd: (newData) =>
                            new Promise((resolve) => {
                                handleRowAdd(newData, resolve)
                            }),
                        onRowDelete: (oldData) =>
                            new Promise((resolve) => {
                                handleRowDelete(oldData, resolve)
                            }),
                    }}
                    options={{
                        grouping: true
                    }}
                />
            </Grid>
            <Grid item xs={1}/>
        </Grid>
    );
}

export default BikeTab;