import React from 'react';

function HomeTab() {
    return (
        <div>
            <h1>BikeClub Management application</h1>
            <p>This web application is intended for internal usage by bike club managers and members</p>
            <p>
                There are a lot of entities and processes you can manage.
                To get started just toggle the main toolbar by clicking on icon at your top left corner :)
            </p>
            <img src={'https://media.istockphoto.com/vectors/vector-illustration-vintage-road-bike-bicycle-symbol-cycling-club-vector-id985388158?k=6&m=985388158&s=170667a&w=0&h=wq2LsB60bY5PeOVDam803D68HD6tuekQP7XLgbDa28U='} alt={'bikeclub'}/>
        </div>
    );
}

export default HomeTab;