import React, { useState, useEffect } from 'react';
import { forwardRef } from 'react';
import Grid from '@material-ui/core/Grid'

import MaterialTable from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import axios from 'axios'
import Alert from '@material-ui/lab/Alert';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const api = axios.create({
    baseURL: `http://localhost:8080/api`
})

function RentalTab() {

    const [data, setData] = useState([]); //table data
    const [members, setMembers] = useState({});
    const [memberLinks, setMemberLinks] = useState({});
    const [employees, setEmployees] = useState({});
    const [employeeLinks, setEmployeeLinks] = useState({});
    const [bikeStorages, setBikeStorages] = useState({})
    const [bikeStorageLinks, setBikeStorageLinks] = useState({})

    const columns = [
        {title: 'Bike', field: 'bikeAtStorage.id', lookup: bikeStorages},
        {title: 'Bike count', field: 'bikeAtStorage.count', editable: 'never'},
        {title: 'Duration in hours', field: 'durationInHours', type: 'numeric'},
        {title: 'Total price', field: 'totalPrice', type: 'numeric'},
        {title: 'Return time', field: 'returnTime', type: "datetime"},
        {title: 'Member', field: 'member.id', lookup: members},
        {title: 'Department', field: 'department', render: (row) => <span>{row.department.name} - {row.department.city.name}</span>, editable: 'never'},
        {title: 'Verified employee', field: 'verifiedEmployee.id', lookup: employees},
        {title: 'Created', field: 'createdAt', editable: 'never', type: 'datetime'},
        {title: 'Updated', field: 'updatedAt', editable: 'never', type: 'datetime'}
    ]

    //for error handling
    const [iserror, setIserror] = useState(false)
    const [errorMessages, setErrorMessages] = useState([])

    useEffect(() => {
        api.get("/rentals")
            .then(async res => {
                let rentals = res.data._embedded.rentals
                for (const x of rentals) {
                    x.member = (await axios.get(x._links.member.href)).data
                    x.verifiedEmployee = (await axios.get(x._links.verifiedEmployee.href)).data
                    x.bikeAtStorage = (await axios.get(x._links.bikeAtStorage.href)).data
                    x.department = (await axios.get(x.bikeAtStorage._links.department.href)).data
                    x.department.city = (await axios.get(x.department._links.city.href)).data
                }
                setData(rentals)

                let allMembers = (await api.get("/members")).data._embedded.members
                let memberObj = {}
                let memberLinks = {}
                allMembers.forEach(x => {
                    memberObj[x.id] = x.firstName + ' ' + x.lastName;
                    memberLinks[x.id] = x._links.self.href
                });
                setMembers(memberObj)
                setMemberLinks(memberLinks)

                let allEmployees = (await api.get("/employees")).data._embedded.employees
                let employeeObj = {}
                let employeeLinks = {}
                allEmployees.forEach(x => {
                    employeeObj[x.id] = x.firstName + ' ' + x.lastName;
                    employeeLinks[x.id] = x._links.self.href
                });
                setEmployees(employeeObj)
                setEmployeeLinks(employeeLinks)

                let allBikesStorages = (await api.get("/bikeStorages")).data._embedded.bikeStorages
                let bikeStorageObj = {}
                let bikeStorageLinks = {}
                for (const x of allBikesStorages) {
                    let bike = (await axios.get(x._links.bike.href)).data
                    let brand = (await axios.get(bike._links.brand.href)).data
                    bikeStorageObj[x.id] = brand.name + ' ' + bike.model + ' ' + bike.releaseYear;
                    bikeStorageLinks[x.id] = x._links.self.href
                }
                setBikeStorages(bikeStorageObj)
                setBikeStorageLinks(bikeStorageLinks)
            })
            .catch(error=>{
                console.log("Error")
            })
    }, [])

    const handleRowUpdate = (newData, oldData, resolve) => {
        let bikeAtStorageId = newData.bikeAtStorage.id
        delete newData.bikeAtStorage
        let memberId = newData.member.id
        delete newData.member
        let verifiedEmployeeId = newData.verifiedEmployee.id
        delete newData.verifiedEmployee
        delete newData.department
        axios.put(oldData._links.self.href, newData)
            .then(async res => {
                newData = res.data
                await axios.put(oldData._links.bikeAtStorage.href, bikeStorageLinks[bikeAtStorageId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.bikeAtStorage = (await axios.get(bikeStorageLinks[bikeAtStorageId])).data
                newData.department = (await axios.get(newData.bikeAtStorage._links.department.href)).data
                newData.department.city = (await axios.get(newData.department._links.city.href)).data
                await axios.put(oldData._links.member.href, memberLinks[memberId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.member = (await axios.get(memberLinks[memberId])).data
                await axios.put(oldData._links.verifiedEmployee.href, employeeLinks[verifiedEmployeeId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.verifiedEmployee = (await axios.get(employeeLinks[verifiedEmployeeId])).data
                const dataUpdate = [...data];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Update failed! Server error"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowAdd = (newData, resolve) => {
        let bikeAtStorageId = newData.bikeAtStorage.id
        delete newData.bikeAtStorage
        let memberId = newData.member.id
        delete newData.member
        let verifiedEmployeeId = newData.verifiedEmployee.id
        delete newData.verifiedEmployee
        delete newData.department
        api.post("/rentals", newData)
            .then(async res => {
                newData = res.data
                await axios.put(res.data._links.bikeAtStorage.href, bikeStorageLinks[bikeAtStorageId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.bikeAtStorage = (await axios.get(bikeStorageLinks[bikeAtStorageId])).data
                newData.department = (await axios.get(newData.bikeAtStorage._links.department.href)).data
                newData.department.city = (await axios.get(newData.department._links.city.href)).data
                await axios.put(res.data._links.member.href, memberLinks[memberId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.member = (await axios.get(memberLinks[memberId])).data;
                await axios.put(res.data._links.verifiedEmployee.href, employeeLinks[verifiedEmployeeId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.verifiedEmployee = (await axios.get(employeeLinks[verifiedEmployeeId])).data
                let dataToAdd = [...data];
                dataToAdd.push(newData);
                setData(dataToAdd);
                resolve()
                setErrorMessages([])
                setIserror(false)
            })
            .catch(error => {
                setErrorMessages(["Cannot add data. Server error!"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowDelete = (oldData, resolve) => {
        axios.delete(oldData._links.self.href)
            .then(res => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1)
                setData([...dataDelete]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Delete failed! Server error"])
                setIserror(true)
                resolve()
            })
    }


    return (
        <Grid container spacing={1}>
            <Grid item xs={1}/>
            <Grid item xs={10}>
                <div>
                    {iserror &&
                    <Alert severity="error">
                        {errorMessages.map((msg, i) => {
                            return <div key={i}>{msg}</div>
                        })}
                    </Alert>
                    }
                </div>
                <MaterialTable
                    title="BikeClub Rentals"
                    data={data}
                    columns={columns}
                    icons={tableIcons}
                    editable={{
                        onRowUpdate: (newData, oldData) =>
                            new Promise((resolve) => {
                                handleRowUpdate(newData, oldData, resolve);
                            }),
                        onRowAdd: (newData) =>
                            new Promise((resolve) => {
                                handleRowAdd(newData, resolve)
                            }),
                        onRowDelete: (oldData) =>
                            new Promise((resolve) => {
                                handleRowDelete(oldData, resolve)
                            }),
                    }}
                    options={{
                        grouping: true
                    }}
                />
            </Grid>
            <Grid item xs={1}/>
        </Grid>
    );
}

export default RentalTab;