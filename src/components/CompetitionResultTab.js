import React, { useState, useEffect } from 'react';
import { forwardRef } from 'react';
import Grid from '@material-ui/core/Grid'

import MaterialTable from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import axios from 'axios'
import Alert from '@material-ui/lab/Alert';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const api = axios.create({
    baseURL: `http://localhost:8080/api`
})

function CompetitionResultTab() {

    const [data, setData] = useState([]); //table data
    const [members, setMembers] = useState({});
    const [memberLinks, setMemberLinks] = useState({});

    const columns = [
        {title: 'Member', field: 'member.id', lookup: members, editable: 'onAdd'},
        {title: 'Time', field: 'time'},
        {title: 'Place', field: 'place', type: 'numeric'},
        {title: 'Event name', field: 'scheduleEntry.event.name', editable: 'never'},
        {title: 'Event date', field: 'scheduleEntry.date', type: 'date', editable: 'never'},
        {title: 'Created', field: 'createdAt', editable: 'never', type: 'datetime'},
        {title: 'Updated', field: 'updatedAt', editable: 'never', type: 'datetime'}
    ]

    //for error handling
    const [iserror, setIserror] = useState(false)
    const [errorMessages, setErrorMessages] = useState([])

    useEffect(() => {
        api.get("/competitionResults")
            .then(async res => {
                let competitionResults = res.data._embedded.competitionResults
                for (const x of competitionResults) {
                    x.eventAttendee = (await axios.get(x._links.eventAttendee.href)).data
                    x.member = (await axios.get(x.eventAttendee._links.member.href)).data
                    x.scheduleEntry = (await axios.get(x.eventAttendee._links.scheduleEntry.href)).data
                    x.scheduleEntry.event = (await axios.get(x.scheduleEntry._links.event.href)).data
                }
                setData(competitionResults);

                let allMembers = (await api.get("/members")).data._embedded.members
                let memberObj = {}
                let memberLinks = {}
                allMembers.forEach(x => {
                    memberObj[x.id] = x.firstName + ' ' + x.lastName;
                    memberLinks[x.id] = x._links.self.href
                });
                setMembers(memberObj)
                setMemberLinks(memberLinks)
            })
            .catch(error=>{
                console.log("Error")
            })
    }, [])

    const handleRowUpdate = (newData, oldData, resolve) => {
        let memberId = newData.member.id
        delete newData.member
        axios.put(oldData._links.self.href, newData)
            .then(async res => {
                newData = res.data
                await axios.put(oldData._links.member.href, memberLinks[memberId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.member = (await axios.get(memberLinks[memberId])).data
                const dataUpdate = [...data];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Update failed! Server error"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowAdd = (newData, resolve) => {
        let memberId = newData.member.id
        delete newData.member
        api.post("/competitionResults", newData)
            .then(async res => {
                newData = res.data
                await axios.put(res.data._links.member.href, memberLinks[memberId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.member = (await axios.get(memberLinks[memberId])).data;
                let dataToAdd = [...data];
                dataToAdd.push(newData);
                setData(dataToAdd);
                resolve()
                setErrorMessages([])
                setIserror(false)
            })
            .catch(error => {
                setErrorMessages(["Cannot add data. Server error!"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowDelete = (oldData, resolve) => {
        axios.delete(oldData._links.self.href)
            .then(res => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1)
                setData([...dataDelete]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Delete failed! Server error"])
                setIserror(true)
                resolve()
            })
    }


    return (
        <div className="App">

            <Grid container spacing={1}>
                <Grid item xs={1}/>
                <Grid item xs={10}>
                    <div>
                        {iserror &&
                        <Alert severity="error">
                            {errorMessages.map((msg, i) => {
                                return <div key={i}>{msg}</div>
                            })}
                        </Alert>
                        }
                    </div>
                    <MaterialTable
                        title="BikeClub Competition Results"
                        data={data}
                        columns={columns}
                        icons={tableIcons}
                        editable={{
                            onRowUpdate: (newData, oldData) =>
                                new Promise((resolve) => {
                                    handleRowUpdate(newData, oldData, resolve);
                                }),
                            onRowAdd: (newData) =>
                                new Promise((resolve) => {
                                    handleRowAdd(newData, resolve)
                                }),
                            onRowDelete: (oldData) =>
                                new Promise((resolve) => {
                                    handleRowDelete(oldData, resolve)
                                }),
                        }}
                    />
                </Grid>
                <Grid item xs={1}/>
            </Grid>
        </div>
    );
}

export default CompetitionResultTab;