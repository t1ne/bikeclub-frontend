import React, { useState, useEffect } from 'react';
import { forwardRef } from 'react';
import Grid from '@material-ui/core/Grid'

import MaterialTable from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import axios from 'axios'
import Alert from '@material-ui/lab/Alert';
import {Add, Delete} from "@material-ui/icons";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Select from "react-select";

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const api = axios.create({
    baseURL: `http://localhost:8080/api`
})

function GroupTab() {

    const [data, setData] = useState([]); //table data
    const [skillLevels, setSkillLevel] = useState({})
    const [slLinks, setSLLinks] = useState({})
    const [trainers, setTrainers] = useState({})
    const [trainerLinks, setTrainerLinks] = useState({})

    const columns = [
        {title: 'Name', field: 'name'},
        {title: 'Skill level', field: 'skillLevel.id', lookup: skillLevels},
        {title: 'Trainer', field: 'trainer.id', lookup: trainers},
        {title: 'Created', field: 'createdAt', editable: 'never', type: 'datetime'},
        {title: 'Updated', field: 'updatedAt', editable: 'never', type: 'datetime'}
    ]

    //for error handling
    const [iserror, setIserror] = useState(false)
    const [errorMessages, setErrorMessages] = useState([])
    const [open, setOpen] = React.useState(false);
    const [members, setMembers] = React.useState([])
    const [activeGroup, setActiveGroup] = React.useState({});
    const [selectedMember, setSelectedMember] = React.useState({});

    useEffect(() => {
        api.get("/groups")
            .then(async res => {
                    let groups = res.data._embedded.groups
                    for (const x of groups) {
                        x.skillLevel = (await axios.get(x._links.skillLevel.href)).data
                        x.trainer = (await axios.get(x._links.trainer.href)).data
                        x.members = (await axios.get(x._links.members.href)).data._embedded.members
                        for (const m of x.members) {
                            m.skillLevel = (await axios.get(m._links.skillLevel.href)).data
                        }
                    }
                    setData(groups);
                    let allSL = (await api.get("/skillLevels")).data._embedded.skillLevels
                    let slObj = {}
                    let slLinks = {}
                    allSL.forEach(x => {
                        slObj[x.id] = x.name;
                        slLinks[x.id] = x._links.self.href
                    });
                    setSkillLevel(slObj)
                    setSLLinks(slLinks)

                    let allTrainers = (await api.get("/employees")).data._embedded.employees
                    let trainerObj = {}
                    let trainerLinks = {}
                    allTrainers.forEach(x => {
                        trainerObj[x.id] = x.firstName + ' ' + x.lastName;
                        trainerLinks[x.id] = x._links.self.href
                    });
                    setTrainers(trainerObj)
                    setTrainerLinks(trainerLinks)

                    let allMembers = (await api.get("/members")).data._embedded.members
                    let memberOptions = allMembers.map(m => ({
                        value: m._links.self.href,
                        label: m.firstName + ' ' + m.lastName
                    }))
                    setMembers(memberOptions)
                }
            )
            .catch(error => {
                console.log(error)
                console.log("Error")
            })
    }, [])

    const handleRowUpdate = (newData, oldData, resolve) => {
        let skillLevelId = newData.skillLevel.id
        delete newData.skillLevel
        let trainerId = newData.trainer.id
        delete newData.trainer
        delete newData.members
        axios.put(oldData._links.self.href, newData)
            .then(async res => {
                newData = res.data
                await axios.put(oldData._links.skillLevel.href, slLinks[skillLevelId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.skillLevel = (await axios.get(slLinks[skillLevelId])).data
                await axios.put(oldData._links.trainer.href, trainerLinks[trainerId], {headers: {
                        'Content-Type': 'text/uri-list'
                    }})
                newData.trainer = (await axios.get(trainerLinks[trainerId])).data
                const dataUpdate = [...data];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Update failed! Server error"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowAdd = (newData, resolve) => {
        let skillLevelId = newData.skillLevel.id
        delete newData.skillLevel
        let trainerId = newData.trainer.id
        delete newData.trainer
        api.post("/groups", newData)
            .then(async res => {
                newData = res.data
                await axios.put(res.data._links.skillLevel.href, slLinks[skillLevelId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.skillLevel = (await axios.get(slLinks[skillLevelId])).data;
                await axios.put(res.data._links.trainer.href, trainerLinks[trainerId], {
                    headers: {
                        'Content-Type': 'text/uri-list'
                    }
                })
                newData.trainer = (await axios.get(trainerLinks[trainerId])).data;
                let dataToAdd = [...data];
                dataToAdd.push(newData);
                setData(dataToAdd);
                resolve()
                setErrorMessages([])
                setIserror(false)
            })
            .catch(error => {
                setErrorMessages(["Cannot add data. Server error!"])
                setIserror(true)
                resolve()
            })
    }

    const handleRowDelete = (oldData, resolve) => {
        axios.delete(oldData._links.self.href)
            .then(res => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1)
                setData([...dataDelete]);
                resolve()
                setIserror(false)
                setErrorMessages([])
            })
            .catch(error => {
                setErrorMessages(["Delete failed! Server error"])
                setIserror(true)
                resolve()
            })
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (selectedOption) => {
        setSelectedMember(selectedOption)
    };

    const handleAddMember = () => {
        axios.post(activeGroup._links.members.href, selectedMember.value, {
            headers: {
                'Content-Type': 'text/uri-list'
            }
        }).then(async res => {
            for (const x of data) {
                x.members = (await axios.get(x._links.members.href)).data._embedded.members
                for (const m of x.members) {
                    m.skillLevel = (await axios.get(m._links.skillLevel.href)).data
                }
            }
        }).catch(error => {
            console.log(error)
            console.log("Error")
        });
        handleClose()
    };

    const groupMembers = (rowData) => {
        const columns = [
            {title: 'First name', field: 'firstName'},
            {title: 'Last name', field: 'lastName'},
            {title: 'Birth date', field: 'birthDate', type: 'date'},
            {title: 'Phone number', field: 'phoneNumber'},
            {title: 'Email', field: 'email'},
            {title: 'Skill level', field: 'skillLevel.name'},
            {title: 'Created', field: 'createdAt', editable: 'never', type: 'datetime'},
            {title: 'Updated', field: 'updatedAt', editable: 'never', type: 'datetime'}
        ]
        return <MaterialTable
            title={'Group members'}
            columns={columns}
            data={rowData.members}
            icons={tableIcons}
            actions={[
                {
                    icon: Delete,
                    tooltip: 'Delete member',
                    onClick: (event, row) => {
                        axios.delete(rowData._links.members.href + '/' + row.id)
                            .then(async res => {
                                for (const x of data) {
                                    x.members = (await axios.get(x._links.members.href)).data._embedded.members
                                    for (const m of x.members) {
                                        m.skillLevel = (await axios.get(m._links.skillLevel.href)).data
                                    }
                                }
                            })
                            .catch(error => console.log(error))
                    }
                }
            ]}/>
    }

    return (
        <Grid container spacing={1}>
            <Grid item xs={1}/>
            <Grid item xs={10}>
                <div>
                    {iserror &&
                    <Alert severity="error">
                        {errorMessages.map((msg, i) => {
                            return <div key={i}>{msg}</div>
                        })}
                    </Alert>
                    }
                </div>
                <MaterialTable
                    title={ 'BikeClub Groups'}
                    data={data}
                    columns={columns}
                    icons={tableIcons}
                    detailPanel={groupMembers}
                    editable={{
                        onRowUpdate: (newData, oldData) =>
                            new Promise((resolve) => {
                                handleRowUpdate(newData, oldData, resolve);
                            }),
                        onRowAdd: (newData) =>
                            new Promise((resolve) => {
                                handleRowAdd(newData, resolve)
                            }),
                        onRowDelete: (oldData) =>
                            new Promise((resolve) => {
                                handleRowDelete(oldData, resolve)
                            }),
                    }}
                    actions={[
                        {
                            icon: Add,
                            tooltip: 'Add member',
                            onClick: (event, rowData) => {
                                setActiveGroup(rowData)
                                handleClickOpen()
                            }
                        }
                    ]}
                />
            </Grid>
            <Grid item xs={1}/>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add member to existing group</DialogTitle>
                <DialogContent style={{height:'350px', width: '300px'}}>
                    <Select onChange={handleChange} options={members}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleAddMember} color="primary">
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
        </Grid>
    );
}

export default GroupTab;